export interface AddressModel {
    id?: number,
    userId: number,
    zipCode: number,
    city: string,
    street: string,
    houseNumber: number
}