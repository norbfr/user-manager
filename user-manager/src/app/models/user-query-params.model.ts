export interface UserQueryParams {
    id?: number,
    firstName?: string,
    lastName?: string,
    email?: string,
    permissions?: string[]
}