import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserQueryParams } from 'src/app/models/user-query-params.model';
import { environment } from 'src/environments/environment';
import { UserModel } from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserApiService {
  private readonly USERS_URL: string = `${environment.apiUrl}users`;

  constructor(private http: HttpClient) { }

  public getUsers(filters: UserQueryParams): Observable<UserModel> {
    return this.http.get<UserModel>(this.USERS_URL, { params: { ...filters }});
  }

  public createUser(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>(this.USERS_URL, user);
  }

  public editUser(user: UserModel): Observable<UserModel> {
    return this.http.put<UserModel>(`${this.USERS_URL}/${user.id}`, user);
  }

  public deleteUser(id: number): Observable<UserModel> {
    return this.http.delete<UserModel>(`${this.USERS_URL}/${id}`);
  }
}