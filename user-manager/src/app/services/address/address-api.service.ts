import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AddressModel } from 'src/app/models/address.model';
import { AddressQueryParams } from 'src/app/models/address-query-params.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddressApiService {
  private readonly ADDRESSES_URL: string = `${environment.apiUrl}addresses`;

  constructor(private http: HttpClient) { }

  public getAddress(filters: AddressQueryParams): Observable<AddressModel> {
    return this.http.get<AddressModel>(this.ADDRESSES_URL, { params: { ...filters } });
  }

  public createAddress(address: AddressModel): Observable<AddressModel> {
    return this.http.post<AddressModel>(this.ADDRESSES_URL, address);
  }

  public editAddress(address: AddressModel): Observable<AddressModel> {
    return this.http.put<AddressModel>(`${this.ADDRESSES_URL}/${address.id}`, address);
  }

  public deleteAddress(id: number): Observable<AddressModel> {
    return this.http.delete<AddressModel>(`${this.ADDRESSES_URL}/${id}`);
  }
}
